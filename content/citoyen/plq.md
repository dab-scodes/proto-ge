---
title: "Information sur le PLQ"
date: 2020-06-30T11:28:57+02:00
Description: "Étudier le tableau de référence"
weight: 1
menu: ["Tableau de référence", "Tableau de consommation"]
---

-
    # Tableau de référence

    ![PLQ initial](../../img/plq.PNG)


-
    # Tableau de consommation

    ![Tableau de consommation](../../img/tableau_de_consommation.PNG)