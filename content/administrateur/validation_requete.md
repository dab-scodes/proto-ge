---
title: "Préaviseurs État"
date: 2020-06-30T11:31:25+02:00
Description: "Validation des requêtes"
weight: 2
menu: [Demander un accès administrateur, Toutes les requêtes, Tableau de consommation]
---

-
    # Demander un accès administrateur

    ![Devenir admin](../../img/devenir_admin.PNG)
    
-
    # Toutes les requêtes

    ![Liste des requêtes](../../img/liste_des_requêtes.png)

-
    # Tableau de consommation

    ![Tableau de consommation](../../img/tableau_de_consommation.PNG)