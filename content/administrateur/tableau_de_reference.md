---
title: "Acteur du PLQ"
date: 2020-06-30T11:29:51+02:00
Description: "Modifier le tableau de référence"
weight: 3
menu: ["Demander un accès administrateur", "Édition locale", "Liste des propositions","Tableau de référence", "Tableau de consommation"]
---

-
    # Demander un accès administrateur

    ![Devenir admin](../../img/devenir_admin.PNG)

-
    # Édition locale

    ![Proposer Modification PLQ](../../img/modifier_tableau.png)

-
    # Liste des propositions

    ![Liste des propositions](../../img/propositions.png)

-
    # Tableau de référence

    ![PLQ](../../img/plq.PNG)


-
    # Tableau de consommation

    ![Tableau de consommation](../../img/tableau_de_consommation.PNG)