---
title: "Chef de projet DDU"
date: 2020-06-30T11:30:55+02:00
Description: "Validation d'un nouveau tableau de référence"
weight: 1
menu: ["Demander un accès administrateur", Les propositions en cours, Tableau de référence, "Tableau de consommation"]
---

-
    # Demander un accès administrateur

    ![Devenir admin](../../img/devenir_admin.PNG)
    
-
    # Les propositions en cours

    ![Liste des requêtes](../../img/propositions.png)

-
    # Tableau de référence

    ![PLQ initial](../../img/plq.PNG)

-
    # Tableau de consommation

    ![Tableau de consommation](../../img/tableau_de_consommation.PNG)
    